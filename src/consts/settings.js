const SETTINGS = {
  START_ALERT_PERIOD: 5000,
  NOTIFICATION_ICON: 'https://cdn3.iconfinder.com/data/icons/forall/1062/gym-512.png',
};

export default SETTINGS;
