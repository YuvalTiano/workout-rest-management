const ENUM = {
  MODAL: {
    SETTINGS: 'SETTINGS',
    UPDATE_TIMER: 'UPDATE_TIMER',
  },
};

export default ENUM;
