const PAGE = {
  INDEX: '/',
  STOPWATCH: '/stopwatch',
  COMING_SOON: '/coming-soon',
};

export default PAGE;
