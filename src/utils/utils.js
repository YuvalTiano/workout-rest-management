export const withTwoDigits = (number) => (number < 10 ? `0${number}` : number);

export const isPushNotificationSupported = () => 'serviceWorker' in navigator && 'PushManager' in window;

export const isNumberBetween = (number, min, max) => {
  if (min && max) {
    return number >= min && number <= max;
  }

  if (min) {
    return number >= min;
  }

  // if (max)
  return number <= max;
};
