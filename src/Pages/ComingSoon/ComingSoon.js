import React from 'react';

const ComingSoon = () => {
  return (
    <span>Coming Soon...</span>
  );
};

export default ComingSoon;
