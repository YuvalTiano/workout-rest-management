import React from 'react';
import PropTypes from 'prop-types';
import Modal from '../../../components/Modal';
import Button from '../../../components/Button';
import ButtonWrapper from '../../../components/Button/ButtonsWrapper';
import { isNumberBetween } from '../../../utils';
import styles from './InputModal.module.scss';

const INPUT = {
  SECONDS: 'SECONDS',
  MINUTES: 'MINUTES',
};

const initialState = { seconds: 0, minutes: 0 };

const InputModal = (props) => {
  const [time, setTime] = React.useState(initialState);

  const handleChange = (type) => (event) => {
    const number = parseInt(event.target.value, 10);

    if (!isNumberBetween(number, 0, 59)) {
      return;
    }

    switch (type) {
      case INPUT.SECONDS:
        setTime((prevTime) => ({
          ...prevTime,
          seconds: number,
        }));
        break;

      case INPUT.MINUTES:
        setTime((prevTime) => ({
          ...prevTime,
          minutes: number,
        }));
        break;

      default:
        break;
    }
  };

  const handleResetButton = () => {
    setTime(initialState);
  };

  const handleApplyButton = () => {
    props.onApply(time);
  };

  return (
    <Modal onExit={ props.onCancel }>
      <div className={ styles.inputModal }>
        <div className={ styles.row }>
          <input
            value={ time.minutes }
            type="number"
            onChange={ handleChange(INPUT.MINUTES) }
            min={ 0 }
            max={ 59 }
          />
          <input
            value={ time.seconds }
            type="number"
            onChange={ handleChange(INPUT.SECONDS) }
            min={ 0 }
            max={ 59 }
          />
        </div>
        <div className={ styles.row }>
          <ButtonWrapper>
            <Button
              title="Apply"
              onClick={ handleApplyButton }
            >
              APPLY
            </Button>
            <Button
              title="Reset"
              onClick={ handleResetButton }
            >
              RESET
            </Button>
            <Button
              title="Cancel"
              onClick={ props.onCancel }
            >
              CANCEL
            </Button>
          </ButtonWrapper>
        </div>
      </div>
    </Modal>
  );
};

InputModal.propTypes = {
  onApply: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
};

export default InputModal;
