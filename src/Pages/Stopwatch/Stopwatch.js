import React from 'react';
import styles from './Stopwatch.module.scss';
import StopwatchLogic from '../../components/StopwatchLogic';
import Button from '../../components/Button';
import ButtonWrapper from '../../components/Button/ButtonsWrapper';
import InputModal from './InputModal';
import NotificationButton from '../../components/NotificationButton';
import SettingsModal from '../../components/SettingsModal';
import {
  withTwoDigits,
  // isPushNotificationSupported,
} from '../../utils';
import beep from '../../utils/beep';
import {
  SETTINGS,
  ENUM,
} from '../../consts';

const TIMER = {
  A: 'TIMER_A',
  B: 'TIMER_B',
};

function Stopwatch() {
  const [isMute, setIsMute] = React.useState(true);
  const [timerATime, setTimeATime] = React.useState({ seconds: 0, minutes: 0 });
  const [timerBTime, setTimeBTime] = React.useState({ seconds: 0, minutes: 0 });
  const [timerToUpdate, setTimerToUpdate] = React.useState(null); // 'A', 'B' or null
  const [currentModal, setCurrentModal] = React.useState(null);

  React.useEffect(() => {
    const timerAdata = JSON.parse(localStorage.getItem(TIMER.A));
    const timerBdata = JSON.parse(localStorage.getItem(TIMER.B));

    if (timerAdata && (timerAdata.seconds || timerAdata.minutes)) {
      setTimeATime(timerAdata);
    }

    if (timerBdata && (timerBdata.seconds || timerBdata.minutes)) {
      setTimeBTime(timerBdata);
    }
  }, []);

  const renderTimer = (timerName) => ({
    time, startTimer, pauseTimer, stopTimer, isRunning,
  }) => {
    // Define if need to make a sound:
    if (!isMute && isRunning) {
      if (time.seconds === 0) {
        beep(50, 500, 250);
      } else if (time.seconds <= SETTINGS.START_ALERT_PERIOD / 1000) {
        beep(50, 500, 50);
      }
    }

    return (
      <div className={ styles.timer }>
        <button
          type="button"
          onClick={ () => setTimerToUpdate(timerName) }
          className={ `${styles.display} ${time.seconds <= SETTINGS.START_ALERT_PERIOD / 1000 ? styles.highlight : undefined}` }
        >
          { `${withTwoDigits(time.minutes)}:${withTwoDigits(time.seconds)}` }
        </button>
        <ButtonWrapper>
          {
            isRunning
              ? (
                <Button
                  isActive
                  onClick={ pauseTimer }
                  className={ styles.controlButton }
                  icon="fa-pause"
                  title="Pause"
                />
              )
              : (
                <Button
                  onClick={ startTimer }
                  className={ styles.controlButton }
                  icon="fa-play"
                  title="Play"
                />
              )
          }
          <Button
            onClick={ stopTimer }
            className={ styles.controlButton }
            icon="fa-stop"
            title="Stop and restart"
          />
        </ButtonWrapper>
      </div>
    );
  };

  const toggleMute = () => {
    setIsMute((prevMute) => !prevMute);
  };

  const closeModal = () => {
    setTimerToUpdate(null);
    setCurrentModal(null);
  };

  const handleModalApplyButton = (time) => {
    switch (timerToUpdate) {
      case TIMER.A:
        // Update Timer:
        setTimeATime(time);

        // Save timer data in the local storage
        localStorage.setItem(TIMER.A, JSON.stringify(time));
        break;

      case TIMER.B:
        // Update Timer:
        setTimeBTime(time);

        // Save timer data in the local storage
        localStorage.setItem(TIMER.B, JSON.stringify(time));
        break;

      default:
        break;
    }

    closeModal();
  };

  const openSettings = () => {
    setCurrentModal(ENUM.MODAL.SETTINGS);
  };

  const renderModal = () => {
    if (timerToUpdate) {
      return (
        <InputModal
          onApply={ handleModalApplyButton }
          onCancel={ closeModal }
        />
      );
    }

    switch (currentModal) {
      case ENUM.MODAL.SETTINGS:
        return (
          <SettingsModal
            closeModal={ closeModal }
          />
        );

      default:
        return null;
    }
  };

  return (
    <div className={ styles.stopwatch }>
      { renderModal() }
      <StopwatchLogic
        seconds={ timerATime.seconds }
        minutes={ timerATime.minutes }
      >
        { renderTimer(TIMER.A) }
      </StopwatchLogic>
      <StopwatchLogic
        seconds={ timerBTime.seconds }
        minutes={ timerBTime.minutes }
      >
        { renderTimer(TIMER.B) }
      </StopwatchLogic>
      <ButtonWrapper>
        <Button
          onClick={ toggleMute }
          icon={ isMute ? 'fa-volume-mute' : 'fa-volume-up' }
          title={ isMute ? 'Mute' : 'Unmute' }
        />
        <NotificationButton />
        <Button
          onClick={ openSettings }
          icon="fa-cog"
          title="Settings"
        />
      </ButtonWrapper>
    </div>
  );
}

export default Stopwatch;
