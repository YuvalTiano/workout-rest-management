import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styles from './NavItem.module.scss';

const NavItem = (props) => {
  return (
    <Link className={ styles.navItem } to={ props.to }>
      <i className={ `fas ${props.icon}` } />
      { props.children }
    </Link>
  );
};

NavItem.propTypes = {
  icon: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
};

// NavItem.defaultProps = {};

export default NavItem;
