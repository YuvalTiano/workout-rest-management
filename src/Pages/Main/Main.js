import React from 'react';
import styles from './Main.module.scss';
import NavItem from './NavItem';
import { PAGE } from '../../consts';

const Main = () => {
  return (
    <div className={ styles.mainContainer }>
      <NavItem
        to={ PAGE.STOPWATCH }
        icon="fa-stopwatch"
      >
        Stopwatch
      </NavItem>
      <NavItem
        to={ PAGE.COMING_SOON }
        icon="fa-map-marker-alt"
      >
        Run Tracker
      </NavItem>
      <NavItem
        to={ PAGE.COMING_SOON }
        icon="fa-cog"
      >
        Settings
      </NavItem>
    </div>
  );
};

export default Main;
