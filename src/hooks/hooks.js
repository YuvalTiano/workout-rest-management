import React from 'react';

export const useKeyEvent = (key, callback) => {
  const cb = React.useRef(callback);

  React.useEffect(() => {
    const handleClick = (event) => {
      if (event.key === key) {
        cb.current();
      }
    };

    // Add event listener:
    document.addEventListener('keydown', handleClick);

    // Remove the event listener:
    return () => {
      document.removeEventListener('keydown', handleClick);
    };
  }, [key]);
};
