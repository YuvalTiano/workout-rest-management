import React from 'react';
import PropTypes from 'prop-types';
import styles from './Modal.module.scss';
import {
  useKeyEvent,
} from '../../hooks';

const Modal = ({
  children,
  onExit,
}) => {
  const closeModal = React.useCallback(() => {
    if (onExit) {
      onExit();
    }
  }, [onExit]);

  useKeyEvent('Escape', closeModal);

  return (
    <div className={ styles.modal }>
      <div className={ styles.body }>
        {
          !!onExit && (
            <button
              type="button"
              className={ styles.exitButton }
              onClick={ onExit }
            >
              <i className="fas fa-times" />
            </button>
          )
        }
        { children }
      </div>
    </div>
  );
};

Modal.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
    PropTypes.element,
    PropTypes.node,
  ]).isRequired,
  onExit: PropTypes.func,
};

Modal.defaultProps = {
  onExit: null,
};

export default Modal;
