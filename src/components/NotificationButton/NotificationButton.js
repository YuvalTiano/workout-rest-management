import React from 'react';
import Button from '../Button';
import { SETTINGS } from '../../consts';
import styles from './NotificationButton.module.scss';

const NotificationButton = () => {
  const [button, setButton] = React.useState({
    icon: Notification.permission === 'granted' ? 'fa-bell' : 'fa-bell-slash',
    isError: false,
  });

  const showNotification = () => {
    // const notify = new Notification('Workout reminder', { // eslint-disable-line
    //   icon: SETTINGS.NOTIFICATION_ICON,
    //   body: 'Time to do a workout!'
    // })

    navigator.serviceWorker.getRegistration()
      .then((reg) => reg.showNotification('Workout reminder', {
        icon: SETTINGS.NOTIFICATION_ICON,
        body: 'Time to do a workout!',
      }))
      .then(() => {
        setButton({
          icon: 'fa-bell',
          isError: false,
        });
      })
      .catch(() => {
        // Show error:
        setButton({
          icon: 'fa-exclamation-circle',
          isError: true,
        });
      });
  };

  const handleClick = async () => {
    if (Notification.permission === 'granted') {
      return showNotification();
    }

    // Show spinner:
    setButton({
      icon: 'fa-spinner fa-spin',
      isError: false,
    });

    // Ask for permission:
    const notificationPermissionStatus = await Notification.requestPermission();
    switch (notificationPermissionStatus) {
      case 'granted':
        return showNotification();

      default:
      case 'default':
      case 'denied':
        return false;
    }
  };

  return (
    <Button
      title="Show Notification"
      onClick={ handleClick }
      icon={ button.icon }
      className={ button.isError ? styles.error : undefined }
    />
  );
};

export default NotificationButton;
