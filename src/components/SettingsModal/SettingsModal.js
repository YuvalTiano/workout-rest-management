import React from 'react';
import PropTypes from 'prop-types';
// import Styled from './SettingsModal.style';
import Modal from '../Modal';

const SettingsModal = ({
  closeModal,
}) => {
  return (
    <Modal
      onExit={ closeModal }
    >
      <div>Coming Soon</div>
    </Modal>
  );
};

SettingsModal.propTypes = {
  closeModal: PropTypes.func.isRequired,
};

// SettingsModal.defaultProps = {};

export default SettingsModal;
