import React from 'react';
import PropTypes from 'prop-types';
import styles from './Button.module.scss';

const Button = (props) => {
  return (
    <button
      type="button"
      className={ `${styles.button} ${props.isActive ? styles.active : undefined} ${props.className}` }
      disabled={ props.disabled }
      onClick={ props.onClick }
      title={ props.title }
      aria-label={ props.title }
    >
      {
        !!props.icon && <i className={ `fas ${props.icon}` } />
      }
      { props.children }
    </button>
  );
};

Button.propTypes = {
  onClick: PropTypes.func.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
    PropTypes.element,
  ]),
  disabled: PropTypes.bool,
  className: PropTypes.string,
  isActive: PropTypes.bool,
  icon: PropTypes.string,
  title: PropTypes.string,
};

Button.defaultProps = {
  disabled: false,
  className: '',
  isActive: false,
  icon: '',
  children: null,
  title: undefined,
};

export default Button;
