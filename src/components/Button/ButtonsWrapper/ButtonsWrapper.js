import React from 'react';
import PropTypes from 'prop-types';
import styles from './ButtonsWrapper.module.scss';

const ButtonsWrapper = ({
  children,
}) => {
  return (
    <div className={ styles.buttonsWrapper }>
      { children }
    </div>
  );
};

ButtonsWrapper.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element.isRequired,
    PropTypes.arrayOf(PropTypes.element.isRequired),
  ]).isRequired,
};

export default ButtonsWrapper;
