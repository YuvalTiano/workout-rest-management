import React from 'react';
import PropTypes from 'prop-types';

const Stopwatch = (props) => {
  const [time, setTime] = React.useState({
    seconds: 0,
    minutes: 0,
  });

  React.useEffect(() => {
    setTime({
      seconds: props.seconds,
      minutes: props.minutes,
    });
  }, [props.seconds, props.minutes, setTime]);

  const interval = React.useRef(false);

  const pauseTimer = () => {
    if (interval.current) {
      clearInterval(interval.current);
      interval.current = false;
    }
  };

  const tick = () => {
    setTime((prevTime) => {
      // Update seconds:
      if (prevTime.seconds > 0) {
        return {
          ...prevTime,
          seconds: prevTime.seconds - 1,
        };
      }

      // Update minutes:
      if (prevTime.minutes > 0) {
        return {
          seconds: 59,
          minutes: prevTime.minutes - 1,
        };
      }

      // Stop watch
      pauseTimer();
      return {
        seconds: 0,
        minutes: 0,
      };
    });
  };

  const startTimer = () => {
    if (!interval.current) {
      interval.current = setInterval(tick, 1000);
    }
  };

  const stopTimer = () => {
    pauseTimer();
    setTime({
      seconds: props.seconds,
      minutes: props.minutes,
    });
  };

  return props.children({
    time, startTimer, pauseTimer, stopTimer, isRunning: !!(interval.current),
  });
};

Stopwatch.propTypes = {
  seconds: PropTypes.number,
  minutes: PropTypes.number,
  children: PropTypes.func.isRequired,
};

Stopwatch.defaultProps = {
  seconds: 0,
  minutes: 0,
};

export default Stopwatch;
