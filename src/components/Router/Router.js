import React from 'react';
import {
  BrowserRouter,
  Switch,
  Route,
} from 'react-router-dom';
import { PAGE } from '../../consts';
import Main from '../../Pages/Main';
import Stopwatch from '../../Pages/Stopwatch';
import ComingSoon from '../../Pages/ComingSoon';

const Router = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route
          exact
          path={ PAGE.STOPWATCH }
          render={ (props) => <Stopwatch { ...props } /> }
        />
        <Route
          exact
          path={ PAGE.COMING_SOON }
          render={ (props) => <ComingSoon { ...props } /> }
        />
        <Route
          exact
          path={ PAGE.INDEX }
          render={ (props) => <Main { ...props } /> }
        />
      </Switch>
    </BrowserRouter>
  );
};

export default Router;
